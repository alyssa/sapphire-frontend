/*
 * Sapphire
 *
 * Copyright (C) 2018 Florrie Haero
 * Copyright (C) 2018 Alyssa Rosenzweig
 * Copyright (C) 2019 eq
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

const modalScreen = document.getElementById('modal-screen');
const modalContent = document.getElementById('modal-content');

window.showModal = function(contentHTML, action) {
    // Modals are for showing forms; contentHTML should contain a <form>, or
    // else a lot of the code here won't work.

    modalScreen.classList.add('visible');

    modalContent.innerHTML = contentHTML;

    const form = modalContent.querySelector('form');

    form.addEventListener('submit', event => {
        event.preventDefault();
        action(form);
    });

    const firstFocusable = modalContent.querySelector(
        // Ripped the useful parts from https://stackoverflow.com/a/7668761/4633828
        'a[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled])'
    );

    if (firstFocusable) {
        firstFocusable.focus();
    }

    const cancelButton = form.elements.cancel;
    if (cancelButton) {
        cancelButton.addEventListener('click', window.hideModal);
    }

    // Return the content element so that the caller can easily add event listeners.
    return modalContent;
};

window.hideModal = () => {
    modalScreen.classList.remove('visible');
};

// Hide the modal when you click the modal screen..
modalScreen.addEventListener('click', window.hideModal);

// ..unless you clicked on the content.
modalContent.addEventListener('click', event => event.stopPropagation());
