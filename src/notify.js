/*
 * Sapphire
 *
 * Copyright (C) 2019 eq
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

// sends a notification if it's enabled in the settings and allowed
// does not process the message at all
window.notify = function(message, options) {
    if ('Notification' in window && window.settings.notifications.send && document.hidden) {
        if (Notification.permission === 'granted') {
            new Notification(message, options);
        } else if (Notification.permission === 'default') {
            Notification.requestPermission().then(function (permission) {
                if (permission === 'granted') {
                    new Notification(message, options);
                }
            });
        }
    }
};
