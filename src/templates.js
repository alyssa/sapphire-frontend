/*
 * Sapphire
 *
 * Copyright (C) 2019 Florrie Haero
 * Copyright (C) 2018 Alyssa Rosenzweig
 * Copyright (C) 2018 eq
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

/* Template source code, a hashmap from the name to the source */

const templateSource = {
    user: `
        <a {{# href}} href="{{ href }}" {{/ href}} class="user" data-id="{{ roomId }}" data-status-id="{{ status.id }}" data-alias="{{ alias }}"
            {{# status.message }}
                title="{{ alias }} ({{ status.name }}) - {{ status.message }}"
            {{/ status.message }}
            {{^ status.message }}
                title="{{ alias }} ({{ status.name }})"
            {{/ status.message }}
        >
            <span class="user-avatar">{{{ avatar }}}</span>
            <span class="user-name">{{ alias }}</span>
            <span class="user-status status-{{ status.id }}">●</span>
        </div>
    `,

    chat: `
        <a href="#{{ id }}" class="chat" data-id="{{ id }}" data-alias="{{ name }}" title="{{ name }}">
            <span class="chat-name">{{ name }}</span>
        </div>
    `,

    messageGroup: `
        <div class="message-group" data-author="{{ authorDetails.id }}">
            <div class="message-group-avatar">{{{ authorDetails.avatar }}}</div>
            <div class="message-group-content">
                {{^compact}}
                    <div class="message-group-heading">
                        <span class="message-author">{{ authorDetails.alias }}</span>
                    </div>
                {{/compact}}
                {{{initial}}}
            </div>
        </div>
    `,

    message: `
        <div class="message">
            {{#compact}}
                <span class="message-author">{{ authorDetails.alias }}:</span>
            {{/compact}}
            <span class="message-content">{{{ contentHTML }}}</span>
        </div>
    `,

    sidebarGroup: `
        <div class="group" data-title="{{ title }}">
            <div class="group-heading">
                <span class="group-title">{{ title }}</span>
            </div>
        </div>
    `,

    avatar: `
        {{#url}}
            <img src="{{ url }}" class="inner-avatar"></img>
        {{/url}}
        {{^url}}
            <svg class="inner-avatar" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width=5 height=5 viewBox="0 0 100 100">
                <rect width="100%" height="100%" fill="{{ color }}" />
                <text fill="var(--select-text-color)" font-size="60px" x="50px" y="50px" dominant-baseline="middle" text-anchor="middle">{{ initial }}</text>
            </svg>
        {{/url}}
    `,

    chatRoomHeader: `
        <div class="room-name-topic-container">
            <div class="room-name">{{ name }}</div>
            <div class="room-topic">{{ topic }}</div>
        </div>
    `,

    buddyRoomHeader: `
        <span class="room-avatar">{{{ avatar }}}</span>
        <div class="room-name-topic-container">
            <div class="room-name">
                {{ alias }}
                <span class="user-status status-{{ status.id }}">●</span>
            </div>
            <div class="room-topic">{{ status.message }}</div>
        </div>
    `,

    accountDropdown: `
        <select name="account">{{# accounts }}
            <option value="{{ id }}">{{ name }} ({{ prplName }})</option>
        {{/ accounts }}</select>
    `,

    modalAddBuddy: `
        <h1>{{#_}}Add a buddy!{{/_}}</h1>
        <p>{{#_}}You'll send a buddy request. Once it's accepted, they'll be added to your buddy list.{{/_}}</p>
        <form>
            <label><span>{{#_}}Account:{{/_}}</span> {{{ accountDropdown }}}</label>
            <label><span>{{#_}}Username:{{/_}}</span> <input name="username" required></label>
            <div class="actions">
                <input type="submit" value="{{#_}}Add{{/_}}">
                <input type="button" name="cancel" value="{{#_}}Cancel{{/_}}">
            </div>
        </form>
    `,

    modalE2E: `
        <h1>{{#_}}Encryption{{/_}}</h1>
        <p>{{#_}}Fingerprints should be compared in real life. Make sure they match!{{/_}}</p>
        <p>
            <strong>
                {{#_}}Your fingerprint:{{/_}}
            </strong>
            {{ourFingerprint}}
        </p>
        <p>
            <strong>
                {{#_}}Their fingerprint:{{/_}}
            </strong>
            {{theirFingerprint}}
        </p>
        <form>
            <label for="verified">
                <input type="checkbox" name="verified" {{#verified}}checked{{/verified}}></input>
                {{#_}}Fingerprints verified{{/_}}
            </label>
            <div class="action">
                <input type="submit" value="{{#_}}Save{{/_}}">
                <input type="button" name="cancel" value="{{#_}}Cancel{{/_}}">
            </div>
        </form>
    `,

    modalChangeAvatar: `
        <h1>{{#_}}Change Avatar{{/_}}</h1>
        <p>This will change your avatar on <strong>{{ accountDetails.name }}</strong>. Here is your current avatar:</p>
        <p><div style="max-width: 100px;">{{{ accountDetails.avatar }}}</div></p>
        <form>
            <div class="actions">
                <input name="file" type="file" accept=".png,.gif,.jpeg,.jpg" required>
                <input type="submit" value="{{#_}}Save{{/_}}">
            </div>
        </form>
    `,

    modalChangeSettings: `
        <h1>Change Settings</h1>
        <h2>Account List</h2>
        <div class="account-list">{{# accounts }}
            <a class="account" data-account-id="{{ id }}" href="#">
                <span class="account-avatar user-avatar">{{{ avatar }}}</span>
                <span class="account-name">{{ name }}</span>
                <span class="account-prpl">({{ prplName }})</span>
            </a>
        {{/ accounts }}</div>
        <form>
            {{{ settings }}}
            <input type="submit" value="{{#_}}Save{{/_}}">
            <input type="button" name="cancel" value="{{#_}}Cancel{{/_}}">
        </form>
    `,

    modalUploadPreview: `
        {{#previewSrc}}
            <div class="upload-preview-container">
                <img src="{{previewSrc}}">
            </div>
        {{/previewSrc}}
        <h1>{{file.name}}</h1>
        {{#remainingString}}
            <p>{{remainingString}}</p>
        {{/remainingString}}
        <div class="full-width">
            <input name="comment" placeholder="{{#_}}Comment (optional){{/_}}">
        </div>
        <form class="actions">
            <input type="submit" value="{{#_}}Upload{{/_}}">
            <input type="button" name="skip" value="{{skipString}}">
        </form>
    `,

    settingHeader: `
        <h{{level}}>{{name}}</h{{level}}>
    `,

    settingBoolean: `
        <label for="{{id}}">
            {{#value}}
                <input type="checkbox" name="{{id}}" checked></input>
            {{/value}}
            {{^value}}
                <input type="checkbox" name="{{id}}"></input>
            {{/value}}
            {{name}}
        </label>
    `,

    icon: `
        <svg id="{{id}}" class="icon">
            <title>{{title}}</title>
            <use href="{{href}}"></use>
        </svg>
    `
};

/* Compile each template and store for global use; mimick the old API via closures */

window.templates = {};

for (const name of Object.keys(templateSource)) {
    const compiled = window.Hogan.compile(templateSource[name]);
    window.templates[name] = function(...data) {
        // Optionally take multiple objects, all of which will be merged
        // together as data passed to the template.
        let flat;
        if (data.length === 1) {
            flat = data[0];
        } else {
            flat = data.reduce((acc, obj) => Object.assign(acc, obj), {});
        }

        // Wire in the i18n function, wrapped in a thunk for hogan.js API
        flat._ = () => window._;

        return compiled.render(flat, templateSource);
    };
}
