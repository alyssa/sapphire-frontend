/*
 * Sapphire
 *
 * Copyright (C) 2018 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

const keyRegex = /{(.+?)}/g;

if (!window.LOCALES) {
    console.warn('po/build.js missing. Run `make` in po/. i18n disabled.');
    window.LOCALES = {};
}

// We always have Canadian English as a fallback
window.LOCALES['en'] = {};

// Choose a default locale
window.LOCALE = navigator.languages
    .map(l => l.split('-')[0])
    .filter(l => !!window.LOCALES[l])[0] || 'en';

/* The main LOCALES object, included in po/build.js, is a dictionary mapping
 * two-letter language codes to localizations.
 *
 * Each localization is itself a dictionary mapping unlocalized (English)
 * template strings to translations.
 *
 * Translations are two-element arrays. The second element is the translation.
 * The first element is seemingly always null. TODO: Strip that since it's a
 * waste of bandwidth.
 *
 * To resolve a template, we first lookup the language code (window.LOCALE) in
 * the LOCALES object, producing a localization object. If the LOCALES object
 * is missing or the desired language is localization, we bail to an empty
 * localization. We then lookup the template in that localization; if it is
 * missing, we bail to an empty translation. Finally, we check is that
 * translation is meaningful (i.e. not an empty string). If it is, we use the
 * translation. If not, we fallback on the original English templates.
 *
 * Example LOCALES:
 *
 * {"es": {"Hello, World": [null, "Hola, Mundo!"]}}
 */

window._ = function(template, substitutionsDict) {
    // Find the locale-appropriate template, defaulting to unlocalized
    const locale = (window.LOCALES && window.LOCALES[window.LOCALE]) || {};
    const entry = locale[template] || [null, ''];
    const localized = entry[1].length ? entry[1] : template;

    // Substitute in {keys} for the actual values
    return localized.replace(keyRegex, (m, key) => substitutionsDict[key]);
};

// Localize HTML

const internationalizable = document.querySelectorAll('[data-i18n]');

for (const el of internationalizable) {
    const prop = el.dataset.i18n;

    if (prop === '') {
        // Inner-text
        el.textContent = window._(el.textContent);
    } else {
        // An attribute
        el.attributes[prop].value = window._(el.attributes[prop].value);
    }
}
