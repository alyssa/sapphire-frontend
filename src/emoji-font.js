/*
 * Sapphire
 *
 * Copyright (C) 2018 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

/* Loads the stylesheet containing Noto Color Emoji, unless we're on a
 * browser with broken font rendering that can't handle it */

const isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
const isBroken = isFirefox; /* My world view in a nutshell */

if (!isBroken) {
    /* We're good to go */

    const link = document.getElementById('emoji-stylesheet');
    link.href = 'style/emoji-noto.css';
}
