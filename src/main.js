/*
 * Sapphire
 *
 * Copyright (C) 2018 Florrie Haero
 * Copyright (C) 2018-2019 Alyssa Rosenzweig
 * Copyright (C) 2018 eq
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

'use strict';

const {
    accountDropdown: tmplAccountDropdown,
    buddyRoomHeader: tmplBuddyRoomHeader,
    chat: tmplChat,
    chatRoomHeader: tmplChatRoomHeader,
    icon: tmplIcon,
    message: tmplMessage,
    messageGroup: tmplMessageGroup,
    sidebarGroup: tmplSidebarGroup,
    user: tmplUser
} = window.templates;

const addBuddyButton = document.getElementById('add-buddy');
const app = document.getElementById('app');
const buddyList = document.getElementById('buddy-list');
const changeSettingsButton = document.getElementById('change-settings');
const chatForm = document.getElementById('chat-form');
const chatList = document.getElementById('chat-list');
const chatTextInput = document.querySelector('[data-id="input"]');
let chatUserList = document.getElementById('chat-user-list');
const e2eSettingsButton = document.getElementById('e2e-settings');
const loginForm = document.getElementById('login-form');
const loginPasswordInput = document.getElementById('password');
const loginUsernameInput = document.getElementById('username');
const loginScreen = document.getElementById('login-screen');
const loginStatus = document.getElementById('login-status');
let messageList = document.getElementById('message-list');
const messageListContainer = document.getElementById('message-list-container');
const roomBackButton = document.getElementById('room-back');
let roomInfoContent = document.getElementById('room-info-content');
const toggleChatUserListButton = document.getElementById('toggle-chat-user-list');
const typingIndicatorName = document.getElementById('typing-indicator-name');
const typingIndicatorStatus = document.getElementById('typing-indicator-status');
const uploadButton = document.getElementById('upload-button');
const uploadInput = document.getElementById('upload-input');

// Maximum width at which the mobile layout (buddy list in a separate view
// from the main chat area) is displayed. MUST be equal to the value specified
// in the @media in style.css.
const MOBILE_WIDTH = 600;

// Values from the API; DO NOT CHANGE without updating the server.
const PURPLE_MESSAGE_SEND = 1;

const userDetailsDict = {};
const chatDetailsDict = {};
const roomDetailsDict = {};
const dmToUser = {};

/* Shadow user object representing us, segregated by account to support
 * varying aliases / avatars / etc */
const selfUser = {};

// The ID of the room you're talking in.
let chatPaneRoomID = null;

// What a MONSTER of a variable name! Tells whether we should switch to the
// chat represented by the location hash (e.g. #prpl-jabber|a/|b). This is
// true only if there even is a location.hash, and then only true until we
// switch to a chat (e.g. because we found the chat, or the user interacted
// before so).
let shouldOpenChatBasedOnInitialHash = location.hash.length > 1;

const removeChildren = function(element) {
    // Does not mutate the element - returns a new element!
    const cNode = element.cloneNode(false);
    element.parentElement.replaceChild(cNode, element);
    return cNode;
};

const isMobileView = () => window.matchMedia(`(min-width: ${MOBILE_WIDTH}px)`);

const getRoomType = function(id) {
    if (id in chatDetailsDict) {
        return 'chat';
    } else if (id in userDetailsDict || id in dmToUser) {
        return 'buddy';
    } else {
        console.warn('Called getRoomType() on a room which is not stored as a chat nor a user: ' + id);
        console.trace();
        return null;
    }
};

/* Either gets a user from the store, or gets ourself */
const getUserByID = function(id, account) {
    if (id === selfUser[account].id) {
        return selfUser[account];
    }

    return userDetailsDict[id];
};

const adjustHistoryOnJoin = function(id) {
    // OoOOoOO, Spooky history tricks!
    if (location.hash !== '#' + id) {
        // Long ago, this was an elegant, single line of code. But everything
        // changed when the Progressive Web Apps attacked. Only the Hacker,
        // master of all four use cases, could sate them, but when the world
        // needed her most, she vanished.
        const newState = {};
        if (isMobileView() && location.hash.length <= 1) {
            newState.openedFromMobileIndex = true;
        }
        history.pushState(newState, null, '#' + id);
    }

    // Now that we've opened a chat, we don't want to automatically switch
    // according to the #hash value again.
    shouldOpenChatBasedOnInitialHash = false;
};

const switchPaneRoomID = function(id) {
    /* Don't try to double switch */
    if (id === chatPaneRoomID) {
        /* We need to switch for mobile anyway */
        hideBuddyPane();

        /* And we need to adjust history anyway */
        adjustHistoryOnJoin(id);

        return;
    }

    const oldChatPaneRoomID = chatPaneRoomID;
    chatPaneRoomID = id;

    // Clear the list of unread messages, stored on the backend, if there are
    // any unread messages. (If there aren't any, there's no need to clear the
    // list - it's already empty!)
    const roomDetails = getRoomDetails(id);
    if (roomDetails.unread) {
        /* MATRIXTODO: Mark as read */
    }

    // Also reset the unread count of this room, so that it isn't highlighted
    // as having unread messages in the buddy list.
    roomDetails.unread = 0;
    updateUnreadIcon();

    // Update classes of affected nodes.
    if (oldChatPaneRoomID) {
        updateRoomClass(oldChatPaneRoomID, 'selected');
    }
    updateRoomClass(id, 'selected');
    updateRoomClass(id, 'unread');

    // Make sure the buddy element is scrolled into view
    const roomEl = getBuddyOrChatEl(id);

    try {
        roomEl.scrollIntoView({behavior: 'instant', block: 'nearest'});
    } catch (e) {
        /* Old Firefox >_> */
        console.log('Suppressing scrolling exception (modern scrollIntoView not supported)');
    }

    // Remove any existing message elements, invalidate the group, then show any messages from that
    // chat.

    /* https://stackoverflow.com/question/3955229/remove-all-child-elements-of-a-dom-node-in-javascript,
     * answer by Maciej Gurban */
    messageList = removeChildren(messageList);

    // Display messages without flushing to improve batch performance
    lastGroup.author = null;
    for (const message of roomDetails.messages) {
        displayMessage(message, false);
    }

    // Then flush at the end.
    flushMessageGroup();

    // Scroll to the bottom. We need to do this once everything has rendered
    // in the browser, so use setTimeout() to run it immediately next "tick"
    // of JavaScript (which will be after the browser has rendered).
    setTimeout(() => {
        scrollToBottom(true);
    });

    // Switch the typing indicator to that of the new pane.
    updateTypingPane();

    // History magic
    adjustHistoryOnJoin(id);

    // Update room info bar.
    updateRoomInfoElement(id);
    updateE2EButton();

    // Do visual updates for mobile layout.
    hideBuddyPane();

    // If we selected a chat, emit that we've now actually joined it.
    if (getRoomType(id) === 'chat') {
        /* MATRIXTODO: JOIN CHAT */

        // Fill in the chat user list, too.
        chatUserList = removeChildren(chatUserList);
        const chatDetails = chatDetailsDict[id];
        const { users } = chatDetails;
        if (users) {
            for (const userID of users.map(u => u.id)) {
                const userDetails = getUserByID(userID, chatDetails.account);
                displayBuddyOrChatItem({
                    // Set an href for clicking to open a DM with buddies
                    html: tmplUser(userDetails, {href: userDetails.isBuddy ? '#' + userID : ''}),
                    listEl: chatUserList,
                    details: userDetails,
                    alias: userDetails.alias,
                    className: 'user',
                    // Since chat users aren't grouped, we want to effectively use
                    // the list element as the group element itself.
                    groupEl: chatUserList
                });
            }
        }

        // Also mark on the DOM that we're viewing a chat - this will cause
        // some CSS changes.
        app.classList.add('chat');
    } else {
        app.classList.remove('chat');
    }
};

const updateRoomInfoElement = function(id) {
    roomInfoContent = removeChildren(roomInfoContent);
    if (getRoomType(id) === 'chat') {
        roomInfoContent.innerHTML = tmplChatRoomHeader(chatDetailsDict[id]);
    } else if (getRoomType(id) === 'buddy') {
        roomInfoContent.innerHTML = tmplBuddyRoomHeader(userDetailsDict[dmToUser[id]]);
    }
};

const updateUnreadIcon = function(x) {
    /* Supplied to save time looking for unread messages */

    let href = '';

    if (x || Object.keys(roomDetailsDict).some(id => roomDetailsDict[id].unread)) {
        href = 'icons/menu.svg#menu-unread';
    } else {
        href = 'icons/menu.svg#menu';
    }

    roomBackButton.lastElementChild.innerHTML = tmplIcon({href: href});
};

const offsetBuddyListIndex = function(amount) {
    let allRoomIDs;
    if (getRoomType(chatPaneRoomID) === 'buddy') {
        const allBuddyEls = Array.from(buddyList.getElementsByClassName('user'));
        allRoomIDs = allBuddyEls.map(el => el.dataset.id);
    } else {
        const allChatEls = Array.from(chatList.getElementsByClassName('chat'));
        allRoomIDs = allChatEls.map(el => el.dataset.id);
    }

    const currentIndex = allRoomIDs.indexOf(chatPaneRoomID);
    let newIndex = currentIndex + amount;
    newIndex = Math.min(allRoomIDs.length - 1, newIndex);
    newIndex = Math.max(0, newIndex);

    const newRoomID = allRoomIDs[newIndex];
    switchPaneRoomID(newRoomID);
};

const getRoomDetails = function(id) {
    if (!(id in roomDetailsDict)) {
        roomDetailsDict[id] = {
            messages: [],
            unread: 0
        };
    }

    return roomDetailsDict[id];
};

const updateUserDetailsDict = function(newDetails) {
    const {id} = newDetails;
    if (id in userDetailsDict) {
        Object.assign(userDetailsDict[id], newDetails);
    } else {
        userDetailsDict[id] = newDetails;
    }

    userDetailsDict[id] = processUser(userDetailsDict[id]);

    if (userDetailsDict[id].isBuddy) {
        displayBuddy(id);
    }
};

const updateChatDetailsDict = function(newDetails) {
    const {id, users = []} = newDetails;
    if (id in chatDetailsDict) {
        Object.assign(chatDetailsDict[id], newDetails);
    } else {
        chatDetailsDict[id] = newDetails;
    }

    // Record any present users in userDetailsDict.
    for (const userDetails of users) {
        if (!(userDetails.id in userDetailsDict)) {
            /* XXX: Get rid of that condition. The issue is that we're
             * preempting the buddy aliases (nondeterministically?) and it's
             * problematic */

            updateUserDetailsDict(userDetails);
        }
    }

    displayChat(id);
};

const onReceiveUserJoinedChat = function(data) {
    const chat = chatDetailsDict[data.chat];

    /* Insert the members */

    if (!chat.users) {
        chat.users = data.members;
    } else {
        /* Only add non-duplicates. TODO: Asymptoptic performance of the
         * functional solution is O(n^2). We probably want a hash table which
         * is O(n) */

        chat.users = chat.users.concat(data.members.filter(mem => chat.users.filter(c => c.id === mem.id).length === 0));
    }

    for (const member of data.members) {
        const userDetails = userDetailsDict[member.id];

        updateUserDetailsDict({
            id: member.id,
            alias: userDetails && userDetails.alias || member.alias,
            chat: data.chat
        });
    }
};

const getBuddyOrChatEl = function(id, listEl = null) {
    const query = `[data-id="${id}"]`;
    if (listEl) {
        return listEl.querySelector(query);
    } else {
        return buddyList.querySelector(query) || chatList.querySelector(query);
    }
};

const getBuddyOrChatGroupEl = function(title, listEl) {
    // NB: This DOES take a listEl!
    return listEl.querySelector(`.group[data-title="${title}"]`);
};

const displayBuddyOrChatItem = function({html, listEl, details, alias, className, groupEl = null}) {
    const {id, group} = details;
    const itemHTML = html;

    if (!group) {
        console.warn('Couldn\'t display a buddy or chat because we don\'t know what group it\'s from. The details we do have:', details);
        return;
    }

    const oldEl = getBuddyOrChatEl(id, listEl);
    if (oldEl) {
        // If there's already an element for this item, just replace it.
        oldEl.insertAdjacentHTML('beforebegin', itemHTML);
        oldEl.remove();
        return;
    }

    // If no groupEl was passed, find the group to put the item in.
    // If there is none, create it.
    if (!groupEl) {
        groupEl = getBuddyOrChatGroupEl(group, listEl);
        if (!groupEl) {
            const groupHTML = tmplSidebarGroup({title: group});
            listEl.insertAdjacentHTML('beforeend', groupHTML);
            groupEl = getBuddyOrChatGroupEl(group, listEl);
        }
    }

    const children = Array.from(groupEl.children);

    // Start at the first element that matches the given className.
    // This is so that we skip past, for example, the group heading.
    const startIndex = children.findIndex(el => el.classList.contains(className));

    // Find the index before which we should insert the new element.
    const aliasA = alias.toLowerCase();
    let i = startIndex;
    if (startIndex >= 0) {
        i = children.slice(startIndex).findIndex(element => {
            const aliasB = element.dataset.alias.toLowerCase();
            return aliasB > aliasA;
        });
        if (i >= 0) {
            i += startIndex;
        }
    }

    // If the index of the element we searched for is at least zero,
    // we found a result, so add the new buddy before that element.
    if (i >= 0) {
        children[i].insertAdjacentHTML('beforebegin', itemHTML);
    } else {
        // Otherwise, it means that every existing buddy should be
        // positioned before this new one (or there just aren't any
        // existing buddies yet), so add the new buddy to the end of
        // the list.
        groupEl.insertAdjacentHTML('beforeend', itemHTML);
    }
};

const displayBuddy = function(id) {
    const userDetails = userDetailsDict[id];

    displayBuddyOrChatItem({
        // Set an href - when this hash is opened (e.g. in a new tab or from a
        // bookmark), it will load this particular buddy's IM chat.
        html: tmplUser(userDetails, {href: '#' + userDetails.roomId}),
        listEl: buddyList,
        details: userDetails,
        alias: userDetails.alias,
        className: 'user'
    });

    updateRoomClass(userDetails.roomId, 'unread');
    updateRoomClass(userDetails.roomId, 'selected');
    updateRoomClass(userDetails.roomId, 'typing');
};

const displayChat = function(id) {
    const chatDetails = chatDetailsDict[id];

    displayBuddyOrChatItem({
        html: tmplChat(chatDetails),
        listEl: chatList,
        details: chatDetails,
        alias: chatDetails.name,
        className: 'chat'
    });
};

/* Updates classes on a buddy object without destroying the whole thing */

const needsClass = function(id, classname) {
    if (classname === 'unread')
        return getRoomDetails(id).unread > 0;
    else if (classname === 'selected')
        return id === chatPaneRoomID;
    else if (classname === 'typing')
        return typingStatus[id] > 0;
    else
        return false;
};

const updateRoomClass = function(id, className) {
    const needed = needsClass(id, className);
    const el = getBuddyOrChatEl(id);

    if (!el) {
        console.warn(`Couldn't update a class of a buddy or chat because its element doesn't exist. (id: ${id}, class: ${className})`);
        return;
    }

    if (needed)
        el.classList.add(className);
    else
        el.classList.remove(className);
};

/* State for last group, to avoid expensive DOM queries while writing messages.
 * Further, we don't even -flush- to the DOM until we really have to */

const lastGroup = {
    author: null,
    container: null,

    queued_html: '',
};

const flushMessageGroup = function() {
    if (lastGroup.queued_html.length > 0 && lastGroup.container) {
        lastGroup.container.insertAdjacentHTML('beforeend', lastGroup.queued_html);
        lastGroup.queued_html = '';
    }
};

/* Applies /me to a message body */

const meify = function(author, msg, emoted) {
    if (!emoted)
        return msg;

    /* This mostly doesn't need to be internationalized, but we expose it for
     * the benefit of RTL languages which flip the order */

    const content = window._('{alias} {me_text}', {
        alias: author.alias,
        me_text: msg
    });

    return `<em>${content}</em>`;
};

const getAuthorDetails = function(message) {
    const isOurMessage = message.flags & PURPLE_MESSAGE_SEND;

    if (isOurMessage) {
        return selfUser["matrix"];
    } else {
        return userDetailsDict[message.who];
    }
};

// show notification for message
const notifyMessage = function(message) {
    const authorDetails = getAuthorDetails(message);
    const meified = meify(authorDetails, message.content, message.emote);
    const sanitized = window.sanitizeHtmlString(meified);

    if (message.chat) {
        const chatDetails = chatDetailsDict[message.chat];

        window.notify(window._('New message from {alias} in {name}', {
            alias: authorDetails.alias,
            name: chatDetails.name,
        }), {
            body: window.settings.notifications.showContent ? sanitized : window._('[content hidden]'),
            icon: window.getAvatarURL(authorDetails),
        });
    } else {
        window.notify(window._('New message from {alias}', {
            alias: authorDetails.alias,
        }), {
            body: window.settings.notifications.showContent ? sanitized : window._('[content hidden]'),
            icon: window.getAvatarURL(authorDetails),
        });
    }
};

const displayMessage = function(message, flush) {
    const authorDetails = getAuthorDetails(message);

    /* Process the message content. This must be client-side to support
     * end-to-end encryption */

    const meified = meify(authorDetails, message.content, message.emote);
    const sanitized = window.sanitizeHtmlString(meified);

    const messageHTML = tmplMessage(Object.assign({}, message, {
        authorDetails,
        contentHTML: sanitized,
    }));

    if (lastGroup.author !== authorDetails.id) {
        /* Flush the old group if we haven't already */
        flushMessageGroup();

        /* Create a new group, with the message at the same time to amortize the cost of creating the group */

        const groupDetails = {authorDetails};
        groupDetails.initial = messageHTML;

        const groupHTML = tmplMessageGroup(groupDetails);
        messageList.insertAdjacentHTML('beforeend', groupHTML);
        lastGroup.author = authorDetails.id;
        lastGroup.container = messageList.lastElementChild.querySelector('.message-group-content');
    } else {
        /* Otherwise, reuse the existing group, appending to the HTML queue */

        lastGroup.queued_html += messageHTML;
    }

    /* For normal (non-replay), flush immediately */

    if (flush) {
        flushMessageGroup();
    }
};

/* Update the E2E button to reflect the current state */
const updateE2EButton = () => {
    const encrypted = false; /* TODO */
    const verified = false; /* TODO */

    e2eSettingsButton.style.display = 'block';

    // Remove existing styles
    e2eSettingsButton.classList.remove('e2e-encrypted');
    e2eSettingsButton.classList.remove('e2e-plaintext');
    e2eSettingsButton.classList.remove('e2e-unverified');

    // Set class based on encryption/verification status
    const e2eClass = encrypted ? (verified ? 'e2e-encrypted' : 'e2e-unverified') : 'e2e-plaintext';

    const classEmoji = {
        'e2e-encrypted': '🔒',
        'e2e-unverified': '🔒?',
        'e2e-plaintext': '🔓'
    };

    e2eSettingsButton.classList.add(e2eClass);
    e2eSettingsButton.innerText = classEmoji[e2eClass];
};

const onReceiveMessage = function(e, flush) {
    const message = {
        flags: e.sender.userId == window.backend.getUserId() ? PURPLE_MESSAGE_SEND : 0,
        room: e.event.room_id,
        content: e.event.content.body,
        who: e.sender.userId,
        emote: e.event.content.msgtype == "m.emote"
    };

    /* While we can ingest plain text, we prefer rich text */

    console.log(e.event.content);
    if (e.event.content.format == "org.matrix.custom.html")
        message.content = e.event.content.formatted_body;

    const isOurMessage = message.flags & PURPLE_MESSAGE_SEND;

    const roomDetails = getRoomDetails(message.room);
    roomDetails.messages.push(message);

    // If we're currently viewing the chat that this message was sent in,
    // immediately display the message.
    if (message.room === chatPaneRoomID) {
        if (flush) {
            const wasScrolledToBottom = isScrolledToBottom(messageList);
            displayMessage(message, true);
            scrollToBottom(wasScrolledToBottom);
        } else {
            displayMessage(message, false);
        }
    } else if (!isOurMessage) {
        // Otherwise, update the unread count - but only if WE didn't send the
        // message! (That could happen if the message was sent from a different
        // client.)
        roomDetails.unread++;
        updateRoomClass(message.room, 'unread');
        console.log("Should update unread");
        updateUnreadIcon(true);
    }

    if (!isOurMessage) {
        notifyMessage(message);
    }
};

/* User -> (int) state */
const typingStatus = {};

/* Updates the active pane to display a typing indicator */
const updateTypingPane = function() {
    // TODO: Support for group chats, somehow
    if (getRoomType(chatPaneRoomID) !== 'buddy') {
        return;
    }

    const buddy = userDetailsDict[dmToUser[chatPaneRoomID]];
    const typingState = typingStatus[chatPaneRoomID];

    typingIndicatorName.innerText = typingState ? buddy.alias : '';
    typingIndicatorStatus.innerText = typingState ? window._('is typing...') : '';
};

const onTyping = function(e, member) {
    /* TODO: Group typing indicators */
    if (!dmToUser[member.roomId])
        return;

    /* Don't display ourselves! */
    if (member.userId == window.backend.getUserId())
        return;

    /* Typing indication occurs in two phases. First, we record the typing
     * status. Second, we actuate it in the GUI if relevant. */

    typingStatus[member.roomId] = member.typing;

    /* Splonk it on the buddy list so CSS can do its thing */

    updateRoomClass(member.roomId, 'typing');

    /* If this buddy is the active pane, update the indicator. */

    if (chatPaneRoomID === member.roomId)
        updateTypingPane();
};

/* Transforms user details from the network to additionally include derived
 * properties suitable for templating */

const processUser = function(details) {
    return Object.assign(details, {
        avatar: window.getAvatar(details)
    });
};

const onReceiveUserDetails = function(details) {
    updateUserDetailsDict(Object.assign(details, {isBuddy: true}));

    if (details.roomId)
        dmToUser[details.roomId] = details.id;

    if (shouldOpenChatBasedOnInitialHash) {
        openChatByHash();
    }
};

const onReceiveChatDetails = function(details) {
    updateChatDetailsDict(details);

    if (shouldOpenChatBasedOnInitialHash) {
        openChatByHash();
    }
};

const onReceiveBuddyStatus = function(data) {
    updateUserDetailsDict({
        id: data.buddy,
        status: data.status
    });

    if (data.buddy === chatPaneRoomID) {
        updateRoomInfoElement(data.buddy);
    }
};

const onReceiveChatTopic = function(e) {
    updateChatDetailsDict({
        id: e.event.room_id,
        topic: e.event.content.topic
    });

    if (e.event.room_id === chatPaneRoomID) {
        updateRoomInfoElement(chatPaneRoomID);
    }
};

const isScrolledToBottom = function(element) {
    const targetY = element.scrollHeight - element.clientHeight;
    const currentY = element.scrollTop;
    const difference = targetY - currentY;
    return difference < 100;
};

const scrollToBottom = function(wasScrolledToBottom) {
    if (wasScrolledToBottom) {
        messageListContainer.scrollTo(0, messageListContainer.scrollHeight);
    }
};

/* Typing indicators, TODO: Less bad, timers, stopped */

function sendTypingState(state) {
    window.backend.sendTyping(chatPaneRoomID, state, 5000);
}

/* Make focus not janky, since when I start typing a message I expect to be
 * able to start, you know, typing a message */

const KEY_TAB = 9;
const KEY_ENTER = 13;
const KEY_SHIFT = 16;
const KEY_ESCAPE = 27;
const KEY_UP = 38;
const KEY_DOWN = 40;
const KEY_V = 86;

const shouldFocusTextInput = function(e) {
    // Don't need to focus it if it's already focused!
    if (document.activeElement === chatTextInput) return false;

    // Capture ctrl-V (paste):
    if ((e.ctrlKey || e.metaKey) && e.which === KEY_V) return true;

    // Don't capture special keys:
    if (e.ctrlKey || e.metaKey || e.altKey) return false;

    // Don't capture tab or enter (by itself):
    if (e.which === KEY_TAB || e.which === KEY_ENTER) return false;

    // Don't capture shift-<nothing>, but do capture anything else <shift>:
    if (e.which === KEY_SHIFT) return false;

    return true;
};

const handleKeyDown = function(event) {
    if (event.which === KEY_DOWN && event.altKey) {
        offsetBuddyListIndex(+1);
        return;
    } else if (event.which === KEY_UP && event.altKey) {
        offsetBuddyListIndex(-1);
        return;
    }

    if (shouldFocusTextInput(event)) {
        chatTextInput.focus();
    }
};

buddyList.addEventListener('keydown', handleKeyDown);
messageListContainer.addEventListener('keydown', handleKeyDown);

chatTextInput.addEventListener('keydown', event => {
    handleKeyDown(event);

    if (event.keyCode === KEY_ENTER) {
        if (!event.shiftKey) {
            event.preventDefault();
            submitChatMessageFromInput();
        }
    }
});

const updateInputHeight = function() {
    const inp = chatTextInput;
    inp.style.height = 'auto';

    const style = window.getComputedStyle(inp, null);
    const rect = inp.getBoundingClientRect();
    const padding = rect.height - parseInt(style.getPropertyValue('height'));

    inp.style.height = (inp.scrollHeight - padding) + 'px';
};

updateInputHeight();
chatTextInput.addEventListener('input', updateInputHeight);

// We use a separate element for the upload button you see and the actual file
// input because it's very difficult (and/or impossible) to use CSS on
// type="file" inputs, and buttons are relatively easy to style. So we need to
// treat clicking the button as clicking the file input.
uploadButton.addEventListener('click', () => {
    uploadInput.click();
});

uploadInput.addEventListener('change', () => {
    const files = Array.from(uploadInput.files);

    const previewFileTypes = [
        'image/bmp',
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/webp'
    ];

    const totalFileCount = files.length;
    const forMultipleFiles = totalFileCount > 1;

    const showFirstFile = () => {
        const file = files[0];
        const hasPreviewImage = previewFileTypes.includes(file.type);
        const html = window.templates.modalUploadPreview({
            file,
            previewSrc: hasPreviewImage && URL.createObjectURL(file),
            skipString: window._(forMultipleFiles ? 'Skip' : 'Cancel'),
            remainingString: forMultipleFiles && window._('({current}/{total}: {remaining} remaining)', {
                current: totalFileCount - files.length + 1,
                total: totalFileCount,
                remaining: files.length - 1
            })
        });

        const submit = () => {
            // TODO: Feedback while we're uploading
            /* MATRIXTODO UPLOAD FILE */
        };

        const modal = window.showModal(html, submit);
        const commentEl = modal.querySelector('[name=comment]');

        commentEl.addEventListener('keydown', evt => {
            if (evt.keyCode === KEY_ENTER) {
                if (!evt.shiftKey) {
                    evt.preventDefault();
                    submit();
                }
            }
        });

        const form = modal.querySelector('form');
        form.elements.skip.addEventListener('click', () => {
            showNextFile();
        });
    };

    const showNextFile = () => {
        files.shift();
        if (files.length) {
            showFirstFile();
        } else {
            window.hideModal();
        }
    };

    showFirstFile();
});

/* Use a global buddy list handler via bubbling, rather than binding to each
 * buddy individually (slow) */

const findParentWithClass = function(e, className) {
    const path = e.path || (e.composedPath && e.composedPath());

    if (path) {
        return path.find(l => l.classList && l.classList.contains(className));
    } else {
        /* Shim for older browsers */

        let target = e.target;

        while (target && !target.classList.contains(className)) {
            console.log(target);
            target = target.parentNode;
        }

        return target;
    }
};

const roomListSwitchHandler = function(e, className) {
    // Prevent "flashing" from the underlying anchor
    e.preventDefault();

    // Only switch if this was a primary-button click without any key modifiers

    if (e.button && e.button !== 0)
        return;

    if (e.ctrlKey || e.shiftKey || e.metaKey)
        return;

    const el = findParentWithClass(e, className);

    if (el) {
        switchPaneRoomID(el.dataset.id);
    }
};

const buddyListSwitchHandler = function(event) {
    roomListSwitchHandler(event, 'user');
};

const chatListSwitchHandler = function(event) {
    roomListSwitchHandler(event, 'chat');
};

buddyList.addEventListener('mousedown', buddyListSwitchHandler);
chatList.addEventListener('mousedown', chatListSwitchHandler);

/* It's not necessary to bind to click, since it's already in the JavaScript
 * href via the hash change */

chatTextInput.addEventListener('input', () => {
    sendTypingState(chatTextInput.value.length > 0);
});

const submitChatMessageFromInput = () => {
    const content = chatTextInput.value;
    chatTextInput.value = '';
    updateInputHeight();

    /* Obviously if we've wiped the input, we're done typing */
    sendTypingState(false);

    return submitChatMessage(content);
};

const submitChatMessage = function(content) {
    /* Apply /me -> emote */
    const emote = content.startsWith("/me ");
    const demoted = emote ? content.slice("/me ".length) : content;

    const optEmojify = window.settings.chat.emojify ? window.emojify : a => a;
    const marked = optEmojify(demoted);
    const processed = window.convertIMMarkdown(marked);

    if (content.length) {
        const proto = emote ? window.backend.sendHtmlEmote
            : window.backend.sendHtmlMessage;

        proto.apply(window.backend, [chatPaneRoomID, marked, processed]).catch((e) => {
            /* TODO: Handle */
            console.error(e);
        });
    }
};

chatForm.addEventListener('submit', event => {
    event.preventDefault();
    submitChatMessageFromInput();
});

const showBuddyPane = function() {
    app.classList.add('show-buddy-list');
    app.classList.remove('show-chat-area');
};

const hideBuddyPane = function() {
    app.classList.remove('show-buddy-list');
    app.classList.add('show-chat-area');
};

const openChatByHash = function() {
    const id = decodeURIComponent(location.hash.slice(1));

    /* No buddy ID means we meant the buddy list, rather than any particular
     * buddy */

    if (id === '') {
        showBuddyPane();
        return;
    }

    /* Otherwise, open the room requested */

    if (id in dmToUser || id in chatDetailsDict) {
        switchPaneRoomID(id);
    }
};

window.addEventListener('popstate', openChatByHash);

roomBackButton.addEventListener('click', event => {
    const state = history.state || {};
    if (state.openedFromMobileIndex) {
        history.back();
    } else {
        location.hash = '#';
    }
    event.preventDefault();
});

toggleChatUserListButton.addEventListener('click', event => {
    app.classList.toggle('show-chat-user-list');
    event.preventDefault();
});

const isE2EVerified = (id, fingerprint) => {
    return false;
};

const setE2EVerified = (id, fingerprint, verified) => {
    console.log("Stub verified");
};

e2eSettingsButton.addEventListener('click', event => {
    const encrypted = false; /* TODO */

    if (encrypted) {
        // We're encrypted, so show the status
        const theirFingerprint = "TODO: FINGERPRINT HERE";

        const html = window.templates.modalE2E({
            ourFingerprint: "TODO: FINGERPRINT HERE",
            theirFingerprint: theirFingerprint,
            verified: isE2EVerified(chatPaneRoomID, theirFingerprint)
        });

        window.showModal(html, form => {
            // Update verified status
            const verified = form.querySelector('[name=verified]').checked;
            setE2EVerified(chatPaneRoomID, theirFingerprint, verified);

            window.hideModal();
            updateE2EButton();
        });
    } else {
        console.log("TODO: DOES THIS MAKE SENSE?");
    }

    event.preventDefault();
});

/* Login screen */

const showLoginScreen = function() {
    loginScreen.classList.add('visible');
};

const hideLoginScreen = function() {
    loginScreen.classList.remove('visible');
};

const showLoginForm = function() {
    loginForm.classList.add('visible');
    if (loginUsernameInput.value) {
        loginPasswordInput.focus();
    } else {
        loginUsernameInput.focus();
    }
};

const hideLoginForm = function() {
    loginForm.classList.remove('visible');
};

const showLoginStatus = function(text) {
    if (loginStatus.firstChild) {
        loginStatus.removeChild(loginStatus.firstChild);
    }

    loginStatus.appendChild(document.createTextNode(text));
    loginStatus.classList.add('visible');
};

const hideLoginStatus = function() {
    loginStatus.classList.remove('visible');
};

const onReceiveAuthError = function(message) {
    gone = false;
    showLoginScreen();
    showLoginStatus(window._(message));
    showLoginForm();

    // Login failed, clear password.
    if (hasLocalStorage)
        localStorage.password = '';
};

const onReceiveAuthSuccess = function() {
    hideLoginScreen();
    hideLoginStatus();

    window.backend.on("sync", function(state, prevState, data) {
        switch (state) {
            case "PREPARED":
                sapphireGetWorld();
                break;
            case "CATCHUP":
            case "SYNCING":
            case "ERROR":
            case "RECONNECTING":
            case "STOPPED":
                /* TODO handle */
                break;
            default:
                console.warn("Unknown state " + state);
                break;
        }
    });

    window.backend.startClient();
};

let in_ratelimit = false;

const onReceiveRatelimit = function(data) {
    /* Slow everything down */
    in_ratelimit = true;

    setTimeout(() => {
        console.log('Clear!\n');

        /* Rate limit is over! */
        in_ratelimit = false;

        /* We're now clear to try again, reveal we were wrong */
        onReceiveAuthError();
    }, data.milliseconds);
};

const onWebSocketClosed = (was_connected) => {
    gone = false;

    /* If we're being ratelimited, don't do anything, since we already know we
     * got closed up on */

    if (in_ratelimit)
        return;

    if (was_connected) {
        showLoginScreen();
        showLoginStatus(window._('Lost connection. Try again soon?'));
        showLoginForm();
    } else {
        showLoginScreen();
        showLoginStatus(window._('Could not connect. Try again soon?'));
        showLoginForm();
    }
};

loginForm.addEventListener('submit', event => {
    event.preventDefault();

    const password = loginPasswordInput.value;
    const username = loginUsernameInput.value;

    /* Clear the password */
    loginPasswordInput.value = '';

    // Save the username and password. If the login fails, we'll clear them.
    if (hasLocalStorage) {
        localStorage.username = username;
        localStorage.password = password;
    }

    go(username, password);
});

changeSettingsButton.addEventListener('click', () => window.showChangeSettings(selfUser));

const makeAccountDropdown = function() {
    const accounts = Object.values(selfUser);
    return tmplAccountDropdown({accounts});
};

// Also hide the modal when you press escape.
document.body.addEventListener('keydown', event => {
    if (event.which === KEY_ESCAPE) {
        window.hideModal();
    }
});

addBuddyButton.addEventListener('click', () => {
    const html = window.templates.modalAddBuddy({
        accountDropdown: makeAccountDropdown()
    });

    window.showModal(html, form => {
        const account = form.querySelector('[name=account]').value;
        const username = form.querySelector('[name=username]').value;
        /* MATRIXTODO buddy request */
        window.hideModal();
    });
});

const onReceiveChangeAvatar = function(data) {
    // Update the account in userDetailsDict and/or selfUser, making note that
    // the user now has an avatar set if they didn't before.
    const account = data.id;
    if (userDetailsDict[account]) {
        // TODO: Rename this to "hasAvatar".
        userDetailsDict[account].hasIcon = true;
        processUser(userDetailsDict[account]);
    }
    if (selfUser[account]) {
        selfUser[account].hasIcon = true;
        processUser(selfUser[account]);
    }

    // Close the change avatar modal if this is the account that had its avatar
    // changed.
    if (account === window.changeAvatarModalAccount) {
        window.hideModal();
        window.changeAvatarModalAccount = null;
    }
};

/* Fire away */

let gone = false;
const go = function(username, password) {
    if (!gone) {
        showLoginScreen();
        showLoginStatus(window._('Logging in...'));
        hideLoginForm();

        /* Splits sunset@equestria.com -> sunset,equestria.com.
         * This is a total hack. TODO: Matrix supports a lot of
         * interactive auth flows, we need UI to support this (or
         * decide we don't want to or whatever) */

        const split = username.split("@");
        const user = split[0];
        const host = split.slice(1).join("@");

        window.backend = matrixcs.createClient({
            baseUrl: "https://" + host
        });

        window.backend.loginWithPassword(
            user, password
        ).then((_) => {
            onReceiveAuthSuccess();
        }).catch((e) => {
            onReceiveAuthError(e.message);
        });

        gone = true;
    }
};

/* We need to check for localstorage since, although it exists in all of our
 * target browsers, it may not be allowed per the content security settings
 * (e.g. in Chromium); attempting to access it otherwise raises a security
 * error. That said, a mere 'localStorage' in window check will not reveal this
 * issue; only an attempt to access it. -AR */

let hasLocalStorage = false;

try {
    localStorage;
    hasLocalStorage = true;
} catch (e) {
    console.warn('WARNING: No local storage...\n');
    console.warn(e);
}

const storedPassword = hasLocalStorage ? localStorage.password : null;
const storedUsername = hasLocalStorage ? localStorage.username : null;
if (storedUsername && storedPassword) {
    // TODO: Error handling. What if the password is wrong?
    go(storedUsername, storedPassword);
} else {
    showLoginScreen();
    showLoginForm();
}

/* Once we're authenticated, get the world */
let gotTheWorld = false;

const sapphireGetWorld = function() {
    /* Don't get the world twice! */
    if (gotTheWorld)
        return;

    gotTheWorld = true;

    const id = window.backend.getUserId();
    const us = window.backend.getUser(id);

    selfUser["matrix"] = processUser({
        alias: us.displayName,
        id: id
    });

    /* Matrix does not have a concept of a buddy list, instead
     * treating everything as a room. There isn't even a proper
     * concept of a direct message yet (canonical DMs are still in
     * the works) -- truly, everything is a room, but m.direct provides some
     * metadata.
     */

    const rooms = window.backend.getRooms();
    const direct = window.backend.getAccountData("m.direct").event.content;

    for (const buddy in direct) {
        const dms = direct[buddy];

        let last = null;
        let last_timestamp = 0;

        /* Create canonical mapping */
        for (const dm of dms) {
            console.log(dm);
            const matches = rooms.filter(x => x.roomId == dm);

            if (!matches.length)
                continue;

            if (matches.length > 1)
                console.error("Too many matches!");

            const match = matches[0];
            const ts = match.getLastActiveTimestamp();

            if (ts > last_timestamp) {
                last_timestamp = ts;
                last = match;
            }
        }

        /* If we found a canonical room, let's add it */

        if (last) {
            const member = last
                .getJoinedMembers()
                .filter(u => u.userId != id)[0];

            const buddy = {
                id: member.userId,
                alias: member.name,
                group: window._("People"),
                roomId: last.roomId
            };

            onReceiveUserDetails(buddy);
        }
    }

    /* Everything else is a group chat */

    for (const room of rooms) {
        if (dmToUser[room.roomId])
            continue;

        const topicEvents = room.currentState.events["m.room.topic"];
        const topicEvent = topicEvents ? topicEvents[""] : undefined;

        const chat = {
            id: room.roomId,
            roomId: room.roomId, /* TODO: unify? */
            name: room.name,
            topic: topicEvent ? topicEvent.event.content.topic : "",
            group: window._("Groups")
        }

        onReceiveChatDetails(chat);
    }

    window.backend.on("Room.timeline", function(e, room, toStartOfTimeline) {
        /* TODO: What is this? */
        if (toStartOfTimeline)
            return;

        if (e.event.type == "m.room.topic") {
            onReceiveChatTopic(e);
        } else if (["m.text", "m.emote"].includes(e.event.content.msgtype)) {
            onReceiveMessage(e, true);
        } else {
            console.warn("Unknown event");
            console.log(e);
        }

    });

    window.backend.on("RoomMember.typing", onTyping);
};

/* Unregister old service worker */
navigator.serviceWorker.getRegistrations().then(registrations => {
    for(const registration of registrations) {
        registration.unregister();
    }
});

//navigator.serviceWorker.register('service-worker.js');
