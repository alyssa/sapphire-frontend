/*
 * Sapphire
 *
 * Copyright (C) 2018 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

/* See attached IM-Markdown spec */

const shruggie = '¯\\_(ツ)_/¯';
const shruggieEscaped = '¯\\\\_(ツ)\\_/¯';

const escaped = /\\([_*~])/g;
const needsEscape = /([_*~])/g;

const doubleAsterisks = /\*\*(.+?)\*\*/g;
const doubleUnderscore = /__(.+?)__/g;
const doubleTilde = /~~(.+?)~~/g;

const singleUnderscore = /_(.+?)_/g;
const singleAsterisk = /\*(\S.*?\S)\*/g;
const singleAsteriskSingleChar = /\*(\S)\*/g;

const links = /https?:\/\/\S+/g;
const email = /(mailto:)?(\S+@\S+\.\S+)/g;

const unescaped = /\|\|ESC(\d)\|\|/g;
const escapes = ['_', '*', '~'];

/* Listing adapting from mustache.js */

const entities = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    '\'': '&#39;',
    '`': '&#x60;',
    '=': '&#x3D;'
};

const escapeHTML = (str) => str.replace(/[&<>"'`=]/g, (s) => entities[s]);

window.convertIMMarkdown = (markdown) => {
    const html = escapeHTML(markdown)
        .split(shruggie).join(shruggieEscaped)
        .replace(links, q => q.replace(needsEscape, '\\$1'))
        .replace(escaped, (q) => `||ESC${escapes.indexOf(q[1])}||`)
        .replace(doubleAsterisks, '<b>$1</b>')
        .replace(doubleUnderscore, '<u>$1</u>')
        .replace(doubleTilde, '<s>$1</s>')
        .replace(singleUnderscore, '<i>$1</i>')
        .replace(singleAsteriskSingleChar, '<i>$1</i>')
        .replace(singleAsterisk, '<i>$1</i>')
        .replace(unescaped, (q) => escapes[q[5]])
        .replace(links, q => `<a href="${q}">${q}</a>`)
        .replace(email, '<a href="mailto:$2">$2</a>');

    return html;
};
