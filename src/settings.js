/*
 * Sapphire
 *
 * Copyright (C) 2018 eq
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

const SETTINGS_MODEL = [
    {
        id: 'chat',
        name: 'Chat',
        type: 'group',
        children: [
            {
                id: 'emojify',
                name: 'Turn emoticons into emoji',
                type: 'boolean',
                default: false
            }
        ]
    },
    {
        id: 'notifications',
        name: 'Notifications',
        type: 'group',
        children: [
            {
                id: 'send',
                name: 'Send notifications',
                type: 'boolean',
                default: true
            },
            {
                id: 'showContent',
                name: 'Show message contents in notifications',
                type: 'boolean',
                default: false
            }
        ]
    }
];

// add new settings from the settings model *without* replacing existing ones
const addDefaultSettings = function(target, model) {
    for (const child of model) {
        if (child.type === 'group') {
            if (!(child.id in target)) {
                target[child.id] = {};
            }
            addDefaultSettings(target[child.id], child.children);
        } else if (!(child.id in target)) {
            target[child.id] = child.default;
        }
    }
};

const loadSettings = function() {
    try {
        window.settings = JSON.parse(localStorage.settings);
    } catch (error) {
        console.error('Could not load string ' + localStorage.settings);
        console.error(error);
        window.settings = {};
    }

    addDefaultSettings(window.settings, SETTINGS_MODEL);
};

const saveSettings = function() {
    localStorage.settings = JSON.stringify(window.settings);
};

const generateSettingsHtml = function() {
    const generateFromModel = function(id, level, model, settings) {
        let out = '';

        for (const item of model) {
            if (item.type === 'group') {
                out += window.templates.settingHeader(
                    { level: level, name: item.name }
                ) + generateFromModel(
                    id + '.' + item.id,
                    level + 1,
                    item.children,
                    settings[item.id]
                );
            } else {
                const funcs = {
                    boolean: window.templates.settingBoolean
                };

                out += funcs[item.type]({
                    id: id + '.' + item.id,
                    name: item.name,
                    value: settings[item.id]
                });
            }
        }

        return out;
    };

    return generateFromModel('settings', 2, SETTINGS_MODEL, window.settings);
};

const handleSettingsForm = function(form) {
    const handleFormModel = function(id, model, settings) {
        for (const item of model) {
            if (item.type === 'group') {
                handleFormModel(
                    id + '.' + item.id,
                    item.children,
                    settings[item.id]
                );
            } else {
                const funcs = {
                    boolean: f => f.checked
                };

                settings[item.id] = funcs[item.type](form.elements[id + '.' + item.id]);
            }
        }
    };

    handleFormModel('settings', SETTINGS_MODEL, window.settings);
    saveSettings();
    window.hideModal();
};

window.showChangeSettings = function(selfUser) {
    const html = window.templates.modalChangeSettings({
        accounts: Object.values(selfUser),
        settings: generateSettingsHtml()
    });

    const modal = window.showModal(html, handleSettingsForm);

    for (const el of modal.getElementsByClassName('account')) {
        const accountDetails = selfUser[el.dataset.accountId];
        el.addEventListener('click', evt => {
            showChangeAvatar(accountDetails);
            evt.preventDefault();
        });
    }
};

window.changeAvatarModalAccount = null;
const showChangeAvatar = function(accountDetails) {
    const html = window.templates.modalChangeAvatar({
        accountDetails
    });

    window.showModal(html, form => {
        const { files } = form.querySelector('[name=file]');
        const file = files[0];

        const reader = new FileReader();

        reader.onload = () => {
            const base64url = reader.result;
            // The base64 URL will start with a data: prefix. We want to send
            // just the data, so remove the prefix.
            const data = base64url.split(',')[1];

            // Set this so we can automatically close the modal once we receive
            // the changeAvatar event for this account. We set it only now that
            // we're actually uploading the new avatar. This is to deal with
            // the corner case of having this modal open while you change your
            // avatar externally (i.e. a separate client/instance/browser tab).
            // The modal on *this* instance - the one where you HAVEN'T yet
            // changed your avatar - won't close.
            // TODO: this is split between two files now, which is a bit ugly?
            window.changeAvatarModalAccount = accountDetails.id;

            /* MATRIXTODO: Change avatar */
        };

        reader.onerror = error => {
            // TODO: Error handling here
            console.error(error);
        };

        reader.readAsDataURL(file);
    });
};

loadSettings();
