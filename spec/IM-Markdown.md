# Instant Messaging Markdown

Instant messaging Markdown is a flavour of Markdown optimized for instant
messaging clients, as opposed to prose composition. Essentially, it is a
small subset of Markdown, with relaxed escaping requirements to accomodate
typical usage patterns. An ad hoc implementation of IM Markdown is found in
chat systems like Discord.

--- 

Unless otherwise noted, plaintext is passed through as plaintext, without any
special escaping needed.

Asterisk matching is to be performed from left to right. For instance, if
asterisks are unbalanced, the rightmost asterisk should be passed through,
rather than the leftmost. Underscore matching is to be performed right to
left, leaving the left-most asterisk as-is.

The string "¯\_(ツ)_/¯" is to be escaped as "¯\\\_(ツ)\_/¯" before processing
as a special-cased emoticon.

Substrings beginning with "http://" or "https://" ending with a space are to be
printed literally, as if all special characters contained were escaped.

A pair of single-asterisks or single-underscores corresponds to italics. If
the character is not matched by itself again unescaped on the correct
boundary, it should be passed through as-is. Single-asterisks must be touching
content: that is, an opening asterisk must have a non-space character to its
right, and a closing asterisk must have a non-space character to its left.
Otherwise, the asterisk is interpreted literally.  Underscores do not carry
this restriction.

A pair of double-asterisks, double underscores, or double-tidles, regardless
of boundaries, (**double** or **longer double**) corresponds to bold,
underline, or strikethrough respectively. Pass through if not matched later in
the text.

A backslash preceding a character with no special meaning in Markdown (like a
letter) is to be printed literally in addition to that character. A backslash
preceding a character with a special meaning in Markdown is to be ignored,
printing that special character literally and ignoring its special function.


