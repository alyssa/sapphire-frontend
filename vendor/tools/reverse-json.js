/* Reverse a JSON dict and lowercases everything */

const fs = require('fs');
const input = JSON.parse(fs.readFileSync(process.argv[2]).toString());

const output = {};

for (const key of Object.keys(input)) {
    output[input[key].toLowerCase()] = key.toLowerCase();
}

fs.writeFileSync(process.argv[3], "window.COUNTRY_NAME_TO_ID = " + JSON.stringify(output) + ";");
