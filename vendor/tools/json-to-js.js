const fs = require('fs');
const jsonA = JSON.parse(fs.readFileSync(process.argv[2]).toString());
const jsonB = JSON.parse(fs.readFileSync(process.argv[3]).toString());
const obj = { ...jsonA, ...jsonB };
fs.writeFileSync(process.argv[4], "window." + process.argv[5] + " = " + JSON.stringify(obj) + ";");
