/*
 * Sapphire
 *
 * Copyright (C) 2018 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

const version = 'v3';

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(version).then(function(cache) {
            return cache.addAll([
                'index.html',

                'style/style.css',

                'vendor/hogan-3.0.1.js',
                'vendor/emoji.js',
                'vendor/countries-rev.js',

                'src/emoji-emoticon-to-unicode.js',
                'src/emojify.js',
                'src/templates.js',
                'src/password.js',
                'src/backend.js',
                'src/main.js',
            ]);
        })
    );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        /*caches.match(event.request).then(function(resp) {
            return resp || fetch(event.request).then(function(response) {
                return caches.open(version).then(function(cache) {
                    cache.put(event.request, response.clone());
                    return response;
                });
            });
        })*/

        fetch(event.request)
    );
});
