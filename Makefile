all:
	cd vendor && make
	cd po && make

clean:
	cd vendor && make clean
	cd po && make clean
