/*
 * Sapphire
 *
 * Copyright (C) 2018 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */

/* Reads a set of *.json files passed as argv, and constructs a JavaScript
 * file exposing them as a global object named LOCALES, where each key is the
 * name of that JSON file with the ending .json cut-off, writing out to build.js */

const fs = require('fs');
const path = require('path');
const files = process.argv.slice(2);

const out = {};

for (const file of files) {
    const parsed = JSON.parse(fs.readFileSync(file).toString());
    out[path.basename(file, '.json')] = parsed;
}

console.log('window.LOCALES=' + JSON.stringify(out));
